package extra;

import java.util.Stack;

public class Tnode {
    private String name;
    private Tnode firstChild;
    private Tnode nextSibling;

    public Tnode(String s) {
        name = s;
    }
    private static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
    @Override
    public String toString() {
        StringBuffer b = new StringBuffer();
        b.append(firstChild == null ? String.valueOf(name) :
                (name + "(" + firstChild + "," + firstChild.nextSibling + ")"));
        return b.toString();
    }

    public static Tnode buildFromRPN (String pol) {
        Stack<Tnode> stack = new Stack<Tnode>();
        String[] arr = pol.trim().split("\\s+");
        int i = 0;
        boolean skipNext = false;
        for (String s : arr) {
            // System.out.println(s+" Before: "+ i + stack); // DEBUG
            if(!skipNext) {
                Tnode me = new Tnode(s);
                if (s.matches("[-+*/]") && stack.size() > 1) {
                    Tnode mychildsibling = stack.pop();
                    Tnode mychild = stack.pop();
                    me.firstChild = mychild;
                    me.firstChild.nextSibling = mychildsibling;
                } else if (s.equals("SWAP") && stack.size() > 1) {
                    try {
                        me.name = arr[i + 1]; // I am next element instead of SWAP
                    } catch ( Exception e) {
                        throw new RuntimeException("No elements after SWAP: " + pol);
                    }
                    skipNext = true;
                    Tnode mychildsibling = stack.pop();
                    Tnode mychild = stack.pop();
                    me.firstChild = mychildsibling;
                    me.firstChild.nextSibling = mychild;
                } else if (s.equals("ROT") && stack.size() > 2) {
                    try {
                        me.name = arr[i + 1]; // I am next element instead of ROT
                    } catch ( Exception e) {
                        throw new RuntimeException("No elements after ROT: " + pol);
                    }
                    skipNext = true;
                    Tnode mychildsibling = stack.pop();
                    Tnode mychild = stack.pop();
                    Tnode ofiamsiblingto = stack.pop();
                    me.firstChild = mychildsibling;
                    me.firstChild.nextSibling = ofiamsiblingto;
                    mychild.nextSibling = me;
                    stack.push(mychild);
                } else if (!isNumeric(s)) {
                    throw new RuntimeException("Possible errors: Illegal symbol or Not enough elements to operate (SWAP/ROT/operator): " + pol);
                }
                stack.push(me);
            } else {
                skipNext = false;
            }
            // System.out.println(s+" After: "+ i + stack); //DEBUG
            i++;
        }

        if (stack.size() > 1) {
            throw new RuntimeException("Too many numbers were entered and not enough operators: " + pol);
        }
        if (stack.isEmpty()) {
            throw new RuntimeException("Too few elements to operate: "+pol);
        }

        return stack.pop();
    }

    public static void main (String[] param) {
        String rpn;
        rpn = "2 3 ROT + / 5 SWAP - -";
        /* TESTS
        rpn = "1 2 3 x +";
        rpn = "1 2 3 5 +";
        rpn = "1 2 3 * +";
        rpn = "1 2 SWAP -";
        rpn = "2 5 9 ROT - +";
        rpn = "2 5 SWAP -";
         */
        System.out.println ("RPN: " + rpn);
        Tnode res = buildFromRPN (rpn);
        System.out.println ("Tree: " + res);
    }
}
