import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   private Tnode(String name, Tnode nextSibling, Tnode firstChild){
      this.name = name;
      this.nextSibling = nextSibling;
      this.firstChild = firstChild;
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      b.append(firstChild == null ? String.valueOf(name) :
              (name + "(" + firstChild + "," + firstChild.nextSibling + ")"));
      return b.toString();

   }
   public static boolean isNumeric(String strNum) {
      try {
         double d = Double.parseDouble(strNum);
      } catch (NumberFormatException | NullPointerException nfe) {
         return false;
      }
      return true;
   }
   public static double op(double d1, double d2,  String s) {
      double res;
      switch (s) {
         case "-":
            res = d1 - d2;
            break;
         case "+":
            res = d1 + d2;
            break;
         case "*":
            res = d1 * d2;
            break;
         case "/":
            res = d1 / d2;
            break;
         default:
            throw new IllegalArgumentException("Unknown operand %s" + s);
      }
      return res;
   }

   public static Tnode buildFromRPN (String pol) {
      String[] ol = pol.trim().split("\\s+");   // split string into array
      LinkedList<Tnode> nodeList = new LinkedList<Tnode>();     // Tree nodes list
      List<Double> tryData = new LinkedList<Double>();      // list for error checking
      int o = 0;
      for(String nodeName : ol){    // check for errors, add nodes to buffer list and calculate stack
         nodeList.add(new Tnode(nodeName, null, null));  // add node to list
         if(isNumeric(nodeName)){    // if string is number
            tryData.add(Double.parseDouble(nodeName));   // add number to calculator stack
         } else if(nodeName.matches("[-+*/]")) {    // if string is operator
            try {    // try to calculate (error check) in stack
               System.out.println(tryData.size());
               double d1 = tryData.get(tryData.size() - 2);
               double d2 = tryData.get(tryData.size() - 1);
               double res = op(d1, d2, nodeName);
               tryData.remove(tryData.size() - 1);
               tryData.remove(tryData.size() - 1);
               tryData.add(res);
            } catch (RuntimeException e) {
               throw new RuntimeException("Too few numbers: " + pol);
            }
         } else if (nodeName.equals("SWAP")){
            String ol1 = ol[o-1];
            ol[o-1] = ol[o-2];
            ol[o-2] = ol1;
            ol[o] = "";
            String newPol = String.join(" ",ol);
            return buildFromRPN(newPol);
         } else if (nodeName.equals("ROT")){
            String ol3 = ol[o-3];
            ol[o-3] = ol[o-2];
            ol[o-2] = ol[o-1];
            ol[o-1] = ol3;
            ol[o] = "";
            String newPol = String.join(" ",ol);
            return buildFromRPN(newPol);
         } else {
            throw new RuntimeException("Incorrect symbol: " + pol);
         }
         System.out.println(pol);
         o++;
      }
      if(tryData.size() > 1){   // if more than 1 number left in stack
         throw new RuntimeException("Too many numbers: " + pol);
      }
      System.out.println("Equation int result: " + tryData.get(0)); // BONUS FUNCTION
      int i = 0;
      Tnode rootNode = nodeList.get(i);
      while (nodeList.size() > 1){
          // Nodes linking process (making a tree)
         String ch = nodeList.get(i).name;
         if ((ch.equals("-") || ch.equals("+") || ch.equals("*") || ch.equals("/")) && i > 1) {
             // given node is parent to previous 2 (that are siblings to each other)
            nodeList.get(i-2).nextSibling = nodeList.get(i-1);
            nodeList.get(i).firstChild = nodeList.get(i-2);
            rootNode = nodeList.get(i); // overwrite every time, last one staying will be root
            nodeList.remove(i-1);
            nodeList.remove(i-2);
            i = 0;
         }
         i++;

      }
      return rootNode;
   }

   public static void main (String[] param) {
      String rpn;
      rpn = "1 2 +";
      rpn = "2 5 SWAP -";
      rpn = "2 5 9 ROT - +";
      rpn = "2 5 9 ROT + SWAP -";
      rpn = "2 5 9 - +";
      /* TESTS (move top comment block)
      rpn = "5 1 - 7 * 6 3 / +";
      rpn = "2   xx";
      rpn = "2   3";
      rpn = "2 -";
      rpn = "x";
      rpn = "2 1   + xx";
      rpn = "+";
      rpn = "2 5 + -";
      rpn = "2    3 + 5";
      rpn = "512 1 - 4 * -61 3 / +";
      */
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN(rpn);
      System.out.println ("Tree: " + res);
   }
}

